import json
import jieba

train_passage = []
valid_passage = []
test_passage = []
valid_query = []
train_query = []
test_query = []
train_answer = []
test_answer = []
valid_answer = []
train_alter = []
valid_alter = []
test_alter = []


def parseJson(file):
    passage =[]
    query =[]
    answer =[]
    alter = []
    with open(file, 'r') as f:
        for line in f.readlines():
            onedata = json.loads(line)
            qList = jieba.cut(onedata["query"])
            qlist = " ".join(qList)
            query.append(qlist)
            pList = jieba.cut(onedata["passage"])
            plist = " ".join(pList)
            passage.append(plist)
            a = onedata["answer"]
            answer.append(a)
            alterList=str(onedata["alternatives"]).split("|")
            alter.append(" ".join(alterList))

    return passage,query,answer,alter
    # print(onedata["url"])


# test_passage,test_query,test_answer=parseJson("../data/ai_challenger_oqmrc_testa.json")
# print(test_answer)
# train_passage,train_query,train_answer,train_alter=parseJson("../data/ai_challenger_oqmrc_trainingset.json")
valid_passage,valid_query,valid_answer,valid_alter=parseJson("../data/ai_challenger_oqmrc_validationset.json")
print(valid_alter)
