from input import readdata
import tensorflow as tf
import numpy as np
import pandas as pd
from inputtest import readtestdata

tX, tY = readdata()
eX, eY = readtestdata()
lr = 0.01
training_iters = 50000
eps = 0.001


def mean_var_with_update(mean, variance):
    ema_apply_op = ema.apply([mean, variance])
    with tf.control_dependencies([ema_apply_op]):
        return tf.identity(mean), tf.identity(mean)


X = tf.placeholder("float", [None, 15])
Y = tf.placeholder("float", [None, 2])

w0 = tf.Variable(tf.truncated_normal([15, 100]) * np.sqrt(2 / 15))
b0 = tf.Variable(tf.constant(0.1, shape=[100, ]))
y0 = tf.matmul(X, w0) + b0
y0a = tf.nn.relu(y0)

axis = list(range(len(w0.shape)-1))
mean, variance = tf.nn.moments(y0a, axes=axis)
scale = tf.Variable(tf.ones([100]))
shift = tf.Variable(tf.zeros([100]))
ema = tf.train.ExponentialMovingAverage(decay=0.5)
mean, var = mean_var_with_update(mean,variance)
y0a = tf.nn.batch_normalization(y0a, mean, var, shift, scale, eps)
y0a = tf.nn.dropout(y0a, keep_prob=0.5)
w1 = tf.Variable(tf.truncated_normal([100, 30]) * np.sqrt(2 / 100))
b1 = tf.Variable(tf.constant(0.1, shape=[30, ]))
y1 = tf.matmul(y0a, w1) + b1
y1a = tf.nn.relu(y1)
axis1 = list(range(len(w1.shape)-1))
mean1, variance1 = tf.nn.moments(y1a, axes=axis)
scale1 = tf.Variable(tf.ones([30]))
shift1 = tf.Variable(tf.zeros([30]))
ema1 = tf.train.ExponentialMovingAverage(decay=0.5)
mean1, var1 = mean_var_with_update(mean1,variance1)
y1a = tf.nn.batch_normalization(y1a, mean1, var1, shift1, scale1, eps)
y1a = tf.nn.dropout(y1a, keep_prob=0.7)
w2 = tf.Variable(tf.truncated_normal([30, 10]) * np.sqrt(2 / 30))
b2 = tf.Variable(tf.constant(0.1, shape=[10, ]))
y2 = tf.matmul(y1a, w2) + b2
y2a = tf.nn.relu(y2)
w3 = tf.Variable(tf.truncated_normal([10, 2]) * np.sqrt(2 / 30))
b3 = tf.Variable(tf.constant(0.1, shape=[2, ]))
y3 = tf.matmul(y2a, w3) + b3
y_final = tf.nn.relu(y3)

cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=Y, logits=y_final))
op = tf.train.AdamOptimizer(lr).minimize(cost)

with tf.Session() as sess:
    init = tf.global_variables_initializer()
    sess.run(init)
    correct_prediction = tf.equal(tf.argmax(y_final, 1), tf.argmax(Y, 1), name="correct_prediction")
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name="accuracy")

    for epoch in range(training_iters):
        avg_cost = 0.
        _, c = sess.run([op, cost], feed_dict={X: tX, Y: tY})
        avg_cost += c
        if (epoch + 1) % 1000 == 0:
            print("Epoch:", '%d' % (epoch + 1), "cost=", "{:.9f}".format(avg_cost))
            print("Accuracy:", accuracy.eval({X: tX, Y: tY}))
        if (epoch + 1) % 1000 == 0:
            y = tf.argmax(y_final.eval({X: eX, Y: eY}), 1)
            y = tf.reshape(y, [418, 1])
            y_y = sess.run(y)
            submiss = pd.read_csv("../data/gender_submission.csv", keep_default_na=True)
            id = submiss.PassengerId;
            y_y = np.reshape(y_y, [418, ]).T
            id = id.T
            c = np.array([id, y_y]).T
            pd_data = pd.DataFrame(c, columns=['PassengerId', 'Survived'])
            pd_data.to_csv("gender_submission_9_" + '%d' % (epoch + 1) + ".csv",index=None)
