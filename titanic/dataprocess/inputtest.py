import pandas as pd
import numpy as np

test_size = 64

train = pd.read_csv("../data/test.csv", keep_default_na=True)
survived, pclass, sex, age, sibsp, parch, ticket, fare, cabin, embarked = train.Pclass, train.Pclass, train.Sex, train.Age, train.SibSp, train.Parch, train.Ticket, train.Fare, train.Cabin, train.Embarked

cabin = cabin.isnull()
cabin = np.reshape(cabin, [cabin.shape[0], 1])
ageboolean = age.isnull()
ageboolean = np.reshape(ageboolean, [ageboolean.shape[0], 1])


def testindices(label):
    test_indices = np.arange(len(label))
    np.random.shuffle(test_indices)
    test_indices = test_indices[0:test_size]
    print(label[test_indices])


def feature(features, name):
    features = np.reshape(features, [features.shape[0], 1])
    if name == 'age':
        agefeature = []
        for i in range(features.shape[0]):
            if ageboolean[i] == True:
                agefeature.append([0, 0])
            else:
                agefeature.append([1 * features[i][0], int((features[i][0] / 10) + 1)])
        agefeature = np.reshape(agefeature, [features.shape[0], 2])
        return agefeature
    if name == 'cabin':
        cabinfeature = []
        for i in range(features.shape[0]):
            if features[i] == True:
                cabinfeature.append([0])
            else:
                cabinfeature.append([1])
        cabinfeature = np.reshape(cabinfeature, [features.shape[0], 1])
        return cabinfeature
    if name == 'embarked':
        embarkedfeature = []
        for i in range(features.shape[0]):
            if features[i] == 'S':
                embarkedfeature.append([1, 0, 0])
            elif features[i] == 'C':
                embarkedfeature.append([0, 1, 0])
            else:
                embarkedfeature.append([0, 0, 1])
        embarkedfeature = np.reshape(embarkedfeature, [features.shape[0], 3])
        return embarkedfeature
    if name == 'pclass':
        pclassfeature = []
        for i in range(features.shape[0]):
            if features[i] == 1:
                pclassfeature.append([1, 0, 0])
            elif features[i] == 2:
                pclassfeature.append([0, 2, 0])
            else:
                pclassfeature.append([0, 0, 3])
        pclassfeature = np.reshape(pclassfeature, [features.shape[0], -1])
        return pclassfeature
    if name == 'sex':
        sexfeature = []
        for i in range(features.shape[0]):
            if features[i] == 'male':
                sexfeature.append([1, 0])
            else:
                sexfeature.append([0, 1])
        sexfeature = np.reshape(sexfeature, [features.shape[0], 2])
        return sexfeature
    if name == 'fare':
        farefeature = []
        for i in range(features.shape[0]):
            farefeature.append([1 * features[i][0], int((features[i][0] / 4) + 1)])
        farefeature = np.reshape(farefeature, [features.shape[0], 2])
        return farefeature
    if name == 'num':
        numfeature = np.reshape(features, [features.shape[0], 1])
        return numfeature
    if name == 'survived':
        survivedfeature = []
        for i in range(features.shape[0]):
            if i % 2 == 0:
                survivedfeature.append([1, 0])
            else:
                survivedfeature.append([0, 1])
        survivedfeature = np.reshape(survivedfeature, [features.shape[0], 2])
        return survivedfeature


pclassfeature = feature(pclass, 'pclass')
sexfeature = feature(sex, 'sex')
agefeature = feature(age, 'age')
sibspfeature = feature(sibsp, 'num')
parchfeature = feature(parch, 'num')
farefeature = feature(fare, 'fare')
cabinfeature = feature(cabin, 'cabin')
embarkedfeature = feature(embarked, 'embarked')
survivedfeature = feature(survived, 'survived')


# print(embarkedfeature)

def combinefeature():
    allfeature = []
    for i in range(pclassfeature.shape[0]):
        onefeature = list(pclassfeature[i]) + list(sexfeature[i]) + list(agefeature[i]) + list(sibspfeature[i]) + list(
            parchfeature[i]) + list(farefeature[i]) + list(cabinfeature[i]) + list(embarkedfeature[i])
        allfeature.append(onefeature)
    return allfeature;


def readtestdata():
    test_feature = combinefeature()
    print("已成功读取数据")
    return test_feature,survivedfeature
