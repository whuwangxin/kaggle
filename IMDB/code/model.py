import tensorflow as tf
from embedding import inputtest, inputtrain, label
import numpy as np
import pandas as pd
import logging
import os

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

trX = inputtrain()
teX = inputtest()
trY = label()
print(trY[0])
print(trY[2])
lr = 0.001
training_iters = 10000
batch_size = 250
maxSeqLength = 200
n_inputs = 200
n_steps = 200
n_hidden_units = 256
n_out = 2
n_classes = 2
num_layers = 3
keep_prob = 0.6
worddim = 200
regularation_param = 0.0001

X = tf.placeholder(tf.float32, [None, n_steps, n_inputs], name='X')
Y = tf.placeholder(tf.float32, [None, 2], name='Y')

weights = {
    'in': tf.Variable(tf.random_normal([n_inputs, n_hidden_units]), name='win'),
    'out': tf.Variable(tf.random_normal([n_hidden_units, n_out]), name='wout'),
    # 'fc': tf.Variable(tf.random_normal([n_out, n_classes]))
}
biases = {
    'in': tf.Variable(tf.constant(0.1, shape=[n_hidden_units, ]), name='bin'),
    'out': tf.Variable(tf.constant(0.1, shape=[n_out, ]), name='bout'),
    # 'fc': tf.Variable(tf.random_normal([n_classes, ]))
}

X_a = tf.reshape(X, [-1, n_inputs])
X_in_a = tf.matmul(X_a, weights['in']) + biases['in']
X_in = tf.reshape(X_in_a, [-1, n_steps, n_hidden_units], name='x_in')

lstm_cell = tf.nn.rnn_cell.BasicLSTMCell(n_hidden_units, forget_bias=1.0, state_is_tuple=True)
lstm_cell = tf.nn.rnn_cell.DropoutWrapper(cell=lstm_cell, input_keep_prob=keep_prob, output_keep_prob=keep_prob)
# 多层RNN
cell = tf.nn.rnn_cell.MultiRNNCell(cells=[lstm_cell] * num_layers, state_is_tuple=True)
init_state = cell.zero_state(batch_size, dtype=tf.float32)

outputs, final_state = tf.nn.dynamic_rnn(cell, inputs=X_in, initial_state=init_state, time_major=False)
h_state = final_state[-1][1]
pred = tf.matmul(h_state, weights['out']) + biases['out']
y_final = pred
# y_final = tf.matmul(pred, weights['fc']) + biases['fc']
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=pred, labels=Y) + regularation_param * (
        tf.nn.l2_loss(weights['in']) + tf.nn.l2_loss(weights['out'])))

train_op = tf.train.AdamOptimizer(lr).minimize(cost)
tf.add_to_collection('train_op', train_op)

correcr_pred = tf.equal(tf.argmax(y_final, 1), tf.argmax(Y, 1))
tf.add_to_collection('pred', correcr_pred)

accuracy = tf.reduce_mean(tf.cast(correcr_pred, tf.float32))
tf.add_to_collection('acc', accuracy)

ckpt_dir = "./model7"
if not os.path.exists(ckpt_dir):
    os.makedirs(ckpt_dir)

global_step = tf.Variable(0, name='global_step', trainable=False)
saver = tf.train.Saver()

with tf.Session() as sess:
    init = tf.global_variables_initializer()
    sess.run(init)
    f = open('../result/record7.txt', 'w')
    for i in range(training_iters):
        startlist = [i for i in range(0, 25000, batch_size)]
        endlist = [i for i in range(batch_size, 25001, batch_size)]
        training_batch = zip(startlist, endlist)
        avg_cost = 0.
        for start, end in training_batch:
            _, c, acc = sess.run([train_op, cost, accuracy], feed_dict={X: trX[start:end], Y: trY[start:end]})
            avg_cost += c
        logging.info("第%d个循环后 loss：%f,准确率：%f" % (i + 1, avg_cost, acc))
        f.write("第%d个循环后 loss：%f,准确率：%f \n" % (i + 1, avg_cost, acc))

        yyy = []
        if ((i + 1) % 100 == 0):

            startlist = [i for i in range(0, 25000, batch_size)]
            endlist = [i for i in range(batch_size, 25001, batch_size)]
            testing_batch = zip(startlist, endlist)
            for start, end in testing_batch:
                y = sess.run([y_final], {X: teX[start:end]})
                y = np.reshape(y, [batch_size, 2])
                y_y = tf.argmax(y, 1)
                y_y = sess.run(y_y)
                yyy.append(y_y)

            yyy = np.reshape(yyy, [-1, 1])
            print(yyy.shape)
            submiss = pd.read_csv("../data/sampleSubmission.csv", keep_default_na=True)
            id = submiss.id;
            y_y = np.reshape(yyy, [25000, ]).T
            id = id.T
            c = np.array([id, y_y]).T
            pd_data = pd.DataFrame(c, columns=['id', 'sentiment'])
            pd_data.to_csv("../result/submission_7_" + '%d' % (i + 1) + ".csv", index=None)
            global_step.assign(i + 1).eval()
            saver.save(sess, ckpt_dir + "/model7.ckpt", global_step=global_step)
    f.close()
