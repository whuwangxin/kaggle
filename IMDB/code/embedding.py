import numpy as np
import re


BASE_DIR = '/home/wangxin/code/'
glovefile = BASE_DIR + 'glove.6B.200d.txt'

worddim=200
maxSeqLength = 200
strip_special_chars = re.compile("[^A-Za-z0-9 ]+")
ZeroWord = np.ones((worddim), dtype='int32')

def cleanSentences(string):
    string = string.lower().replace("<br />", " ")
    return re.sub(strip_special_chars, "", string.lower())


def embeddinng():
    f = open(glovefile)
    embeddings_index = {}
    for line in f:
        values = line.split()
        word = values[0]
        vector = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = vector
    f.close()
    print('Found %s word vectors.' % len(embeddings_index))
    return embeddings_index


embeddings_index = embeddinng()


def data(f):
    with open(f) as f:
        tx = []
        alline = f.readlines()
        for line in alline:

            cleanedLine = cleanSentences(line)
            split = cleanedLine.split()
            # split.reverse()
            # # print("%d,%d"%(len(split),len(a)))
            # print(split)

            # print(split)
            onereview = np.zeros([maxSeqLength, worddim], dtype='float32')
            indexCounter = 0

            for word in split:
                try:
                    # print(word)
                    onereview[indexCounter] = embeddings_index[word]
                except KeyError:
                    onereview[indexCounter] = ZeroWord  # Vector for unknown words
                indexCounter = indexCounter + 1
                if (indexCounter >= maxSeqLength):
                    break
            tx.append(onereview)

        data = np.reshape(tx, [tx.__len__(), maxSeqLength, worddim])
        return data
#
#
# testtest = data('../data/testfile.txt')
# print(testtest)

def inputtrain():
    tX = data('../data/tX.txt')
    print(tX.shape)
    return tX
def inputtest():
    eX = data('../data/eX.txt')
    print(eX.shape)
    return eX


def label():
    label = []
    f = open('../data/Label.txt')
    alline = f.readlines()
    for line in alline:
        if (line.find('1')):
            label.append([0, 1])
        if (line.find('0')):
            label.append([1, 0])
    return label
