import pandas as pd
import numpy as np
import re
from bs4 import BeautifulSoup
from keras.datasets import imdb


# 载入数据集
train = pd.read_csv('/Users/unclewang/PycharmProjects/kaggle/IMDB/data/labeledTrainData.tsv', header=0, delimiter="\t",
                    quoting=3)
test = pd.read_csv('/Users/unclewang/PycharmProjects/kaggle/IMDB/data/testData.tsv', header=0, delimiter="\t",
                   quoting=3)


def review_to_wordlist(review):
    '''
    把IMDB的评论转成词序列
    参考：http://blog.csdn.net/longxinchen_ml/article/details/50629613
    '''
    # 去掉HTML标签，拿到内容
    review_text = BeautifulSoup(review, "html.parser").get_text()
    # 用正则表达式取出符合规范的部分
    review_text = re.sub("[^a-zA-Z]", " ", review_text)
    # 小写化所有的词，并转成词list
    words = review_text.lower().split()
    # 返回words
    return words


label = train.sentiment
# print(label[1])
# f = open('Label.txt', "w+")
# for i in range(label.shape[0]):
#     f.write(str(label[i])+"\n")
# f.close()
#
train_data = []
for i in range(len(train['review'])):
    train_data.append(' '.join(review_to_wordlist(train['review'][i])))
test_data = []
for i in range(len(test['review'])):
    test_data.append(' '.join(review_to_wordlist(test['review'][i])))

print("pre-process data finished!!")
# f = open('tX.txt', "w+")
# for i in range(label.shape[0]):
#     f.write("\n".join(train_data))
# f.close()
def data():

    return train_data, test_data, label
