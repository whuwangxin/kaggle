import pandas as pd
import numpy as np

train = pd.read_csv("../data/test.csv", keep_default_na=True)

MSSubClass = train.MSSubClass
LotFrontage = train.LotFrontage
LotArea = train.LotArea
LotShape = train.LotShape
LandContour = train.LandContour
HouseStyle = train.HouseStyle
OverallQual = train.OverallQual
OverallCond = train.OverallCond
YearBuilt = train.YearBuilt
YearRemodAdd = train.YearRemodAdd
MasVnrArea = train.MasVnrArea
BsmtFinSF1 = train.BsmtFinSF1
BsmtUnfSF = train.BsmtUnfSF
TotalBsmtSF = train.TotalBsmtSF
CentralAir = train.CentralAir
firstFlrSF = train.firstFlrSF
secondFlrSF = train.secondFlrSF
GrLivArea = train.GrLivArea
BsmtFullBath = train.BsmtFullBath
BsmtHalfBath = train.BsmtHalfBath
FullBath = train.FullBath
HalfBath = train.HalfBath
BedroomAbvGr = train.BedroomAbvGr
KitchenAbvGr = train.KitchenAbvGr
TotRmsAbvGrd = train.TotRmsAbvGrd
Fireplaces = train.Fireplaces
GarageCars = train.GarageCars
GarageArea = train.GarageArea
WoodDeckSF = train.WoodDeckSF
PavedDrive = train.PavedDrive
MoSold = train.MoSold
SalePrice = train.MoSold
OpenPorchSF = train.OpenPorchSF
FireplaceQu = train.FireplaceQu
SaleCondition = train.SaleCondition
RoofStyle = train.RoofStyle
LotConfig = train.LotConfig
BsmtQual = train.BsmtQual
ExterQual = train.ExterQual
KitchenQual = train.KitchenQual


def list2set2list(features):
    features = np.reshape(features, [-1])
    newfeatures = np.reshape(list(set(features)), [-1])
    return newfeatures


def feature(features, name):
    features = np.reshape(features, [features.shape[0], 1])
    if name == 'Y':
        Yf = []
        for i in range(features.shape[0]):
            Yf.append(np.math.log(features[i]))
        Yf = np.reshape(Yf, [features.shape[0], 1])
        return Yf
    if name == 'num':
        numfeature = np.reshape(features, [features.shape[0], 1])
        return numfeature
    if name == 'set':
        finalfeature = []
        setfeature = list2set2list(features)
        for i in range(features.shape[0]):
            for j in range(setfeature.shape[0]):
                onefeature = np.zeros(shape=setfeature.shape[0])
                if (features[i] == setfeature[j]):
                    onefeature[j] = 1
                    finalfeature.append(onefeature)
                    break
                else:
                    continue
        finalfeature = np.reshape(finalfeature, [features.shape[0], setfeature.shape[0]])
        return finalfeature
    if name == 'LotFrontage':
        LotFrontage = []
        for i in range(features.shape[0]):
            if features[i] == np.nan:
                LotFrontage.append([0])
            else:
                LotFrontage.append(features[i])
        LotFrontage = np.reshape(LotFrontage, [features.shape[0], 1])
        return LotFrontage


def initfeature():
    feature0 = feature(MSSubClass, 'num')
    feature1 = feature(LotArea, 'num')
    feature2 = feature(OverallQual, 'num')
    feature3 = feature(OverallCond, 'num')
    feature4 = feature(YearBuilt, 'num')
    feature5 = feature(YearRemodAdd, 'num')
    feature6 = feature(MasVnrArea, 'num')
    feature7 = feature(BsmtFinSF1, 'num')
    feature8 = feature(BsmtUnfSF, 'num')
    feature9 = feature(TotalBsmtSF, 'num')
    feature10 = feature(firstFlrSF, 'num')
    feature11 = feature(secondFlrSF, 'num')
    feature12 = feature(GrLivArea, 'num')
    feature13 = feature(BsmtFullBath, 'num')
    feature14 = feature(BsmtHalfBath, 'num')
    feature15 = feature(FullBath, 'num')
    feature16 = feature(HalfBath, 'num')
    feature17 = feature(BedroomAbvGr, 'num')
    feature18 = feature(TotRmsAbvGrd, 'num')
    feature19 = feature(GarageCars, 'num')
    feature20 = feature(GarageArea, 'num')
    feature21 = feature(WoodDeckSF, 'num')
    feature22 = feature(OpenPorchSF, 'num')
    feature23 = feature(MoSold, 'num')
    feature26 = feature(LotFrontage, 'num')
    feature27 = feature(LotShape, 'set')
    feature28 = feature(LandContour, 'set')
    feature29 = feature(HouseStyle, 'set')
    feature30 = feature(CentralAir, 'set')
    feature31 = feature(LotConfig, 'set')
    feature32 = feature(PavedDrive, 'set')
    feature33 = feature(SaleCondition, 'set')
    feature34 = feature(RoofStyle, 'set')
    feature35 = feature(LotConfig, 'set')
    # feature36 = feature(BsmtQual, 'set')
    feature37 = feature(ExterQual, 'set')
    # feature38 = feature(KitchenQual, 'set')
    Y = feature(SalePrice, 'num')
    allfeature = []
    for i in range(feature0.shape[0]):
        onefeature = list(feature0[i]) + list(feature1[i]) + list(feature2[i]) + list(feature3[i]) + list(
            feature6[i]) + list(feature7[i]) + list(feature8[i]) + list(
            feature9[i]) + list(feature10[i]) + list(feature11[i]) + list(feature12[i]) + list(feature13[i]) + list(
            feature14[i]) + list(feature15[i]) + list(feature16[i]) + list(feature17[i]) + list(feature18[i]) + list(
            feature19[i]) + list(feature20[i]) + list(feature21[i]) + list(feature22[i]) + list(feature23[i]) + list(
            feature4[i]) + list(feature5[i]) + list(feature26[i]) + list(feature27[i]) + list(feature28[i]) + list(
            feature30[i]) + list(feature31[i]) + list(feature32[i]) + list(feature33[i]) + list(
            feature34[i]) + list(feature35[i])  + list(
            feature37[i])
        allfeature.append(onefeature)
    return allfeature, Y;
