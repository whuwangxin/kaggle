import tensorflow as tf
import numpy as np
import pandas as pd

import input
import inputtest

lr = 0.003
eps = 0.001
training_iters = 50000
tX, tY = input.initfeature()
eX, eY = inputtest.initfeature()
print(tY)
X = tf.placeholder("float", [None, 64])
Y = tf.placeholder("float", [None, 1])
# def mean_var_with_update(mean, variance):
#     ema = tf.train.ExponentialMovingAverage(decay=0.5)
#     ema_apply_op = ema.apply([mean, variance])
#     with tf.control_dependencies([ema_apply_op]):
#         return tf.identity(mean), tf.identity(mean)
# axis = list(range(1))
# mean, variance = tf.nn.moments(X, axes=axis)
# scale = tf.Variable(tf.ones([24]))
# shift = tf.Variable(tf.zeros([24]))
# mean, var = mean_var_with_update(mean, variance)
# x = tf.nn.batch_normalization(X, mean, var, shift, scale, eps)
w0 = tf.random_normal([64, 30])
b0 = tf.zeros(shape=[30, ])
y0 = tf.matmul(X, w0) + b0
y0a = tf.nn.relu(y0)
w2 = tf.random_normal([30, 10])
b2 = tf.zeros(shape=[10, ])
y2 = tf.matmul(y0a, w2) + b2
y2a = tf.nn.relu(y2)
w1 = tf.random_normal([10, 1])
b1 = tf.Variable(tf.zeros(shape=[1, ]))
y1 = tf.matmul(y2a, w1) + b1

cost = tf.reduce_mean(np.power(y1 - Y, 2))
op = tf.train.AdamOptimizer(lr).minimize(cost)

with tf.Session() as sess:
    init = tf.global_variables_initializer()
    sess.run(init)
    for epoch in range(training_iters):
        _, c = sess.run([op, cost], {X: tX, Y: tY})
        if (epoch + 1) % 1000 == 0:
            print(c)
        if (epoch + 1) % 10000 == 0:
            yy = sess.run([y1], {X: eX})
            submiss = pd.read_csv("../data/sample_submission.csv", keep_default_na=True)
            id = submiss.Id
            y_y = np.reshape(yy, [1459, ]).T
            id = id.T
            c = np.array([id, y_y], dtype='int32').T
            pd_data = pd.DataFrame(c, columns=['Id', 'SalePrice'])
            pd_data.to_csv("sample_submission_2_" + '%d' % (epoch + 1) + ".csv", index=None)
            print(y_y)

    print('loss is ', sess.run(cost, {X: tX, Y: tY}))
