from dataprocess import read
import tensorflow as tf
import numpy as np


tY,tX,eY,eX = read()
print(eY.__len__())
lr = 0.01
training_iters = 20
eps = 0.001

X = tf.placeholder("float", [None, 50])
Y = tf.placeholder("float", [None, 3])

w0 = tf.Variable(tf.truncated_normal([50, 10]) * np.sqrt(2 / 50))
b0 = tf.Variable(tf.constant(0.1, shape=[10 ]))
y0 = tf.matmul(X, w0) + b0
y0a = tf.nn.relu(y0)
y0a = tf.nn.dropout(y0a, keep_prob=0.8)
w1 = tf.Variable(tf.truncated_normal([10, 3]) * np.sqrt(2 / 10))
b1 = tf.Variable(tf.constant(0.1, shape=[3 ]))
y1 = tf.matmul(y0a, w1) + b1
y_final = tf.nn.relu(y1)

cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=Y, logits=y_final))
op = tf.train.AdamOptimizer(lr).minimize(cost)

with tf.Session() as sess:
    init = tf.global_variables_initializer()
    sess.run(init)
    correct_prediction = tf.equal(tf.argmax(y_final, 1), tf.argmax(Y, 1), name="correct_prediction")
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32), name="accuracy")
    for epoch in range(training_iters):
        avg_cost = 0.
        _, c = sess.run([op, cost], feed_dict={X: tX, Y: tY})
        avg_cost += c
        if (epoch + 1) % 10 == 0:
            print("Epoch:", '%d' % (epoch + 1), "cost=", "{:.9f}".format(avg_cost))
            print("Accuracy:", accuracy.eval({X: tX, Y: tY}))
        if (epoch + 1) % 10 == 0:
            y = tf.argmax(y_final.eval({X: eX, Y: eY}), 1)


            print(y)
            # y = tf.reshape(y, [38, 1])
            y_y = sess.run(y)

            print(y_y)

