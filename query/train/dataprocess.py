from typing import List

import numpy as np

classes = []  # 存储文档的类别
preclasses = []
vocab = []  # 存储单词
features = []  # 存储文档向量
prefeatures = []
z = np.zeros((50,))  # 初始化向量
f= open('/home/wangxin/PycharmProjects/kaggle/query/data/vec50.txt', 'r')
num = 0;
for line in f.readlines():
    segment = line.strip().split('\t')

    vocab.append(segment[1])
    # print(segment[1])
    # print(segment[2])
    for j in range(2, len(segment)):  # 每一短语的词向量处理
        voclist = []  # 存储一个词向量的list
        for i in segment[j].strip(' ').split(' '):
            voclist.append(i)
        x = np.array(voclist, dtype='float64')

        z += x
    z = z / (len(segment) - 2)  # 每个短语的均值词向量



    num = num + 1
    if(num<161):
        if(segment[0]==0):
            classes.append([1,0,0])
        elif(segment[0]==1):
            classes.append([0,1,0])
        else:
            classes.append([0,0,1])
        features.append(z.tolist())
    else:
        if (segment[0] == 0):
            preclasses.append([1, 0, 0])
        elif (segment[0] == 1):
            preclasses.append([0, 1, 0])
        else:
            preclasses.append([0, 0, 1])
        prefeatures.append(z.tolist())
    z = np.zeros((50,))

def read():
    print(preclasses.__len__())
    return classes,features,preclasses,prefeatures
read()